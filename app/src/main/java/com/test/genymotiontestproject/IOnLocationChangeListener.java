package com.test.genymotiontestproject;

import android.location.Location;

/**
 * Interface for track location changes.
 * Activity can subscribe on location events and provide them for others view.
 */
public interface IOnLocationChangeListener {
  /**
   * Invoked every time location received.
   *
   * @param newLocation new location.
   */
  void onLocationUpdated(Location newLocation);

  /**
   * Invoked after onLocationUpdated if new location is different from last stored.
   *
   * @param newLocation really new location.
   */
  void onLocationChanged(Location newLocation);

  /**
   * Invoked when permissions required.
   */
  void onLocationPermissionRequired();

  /**
   * Invoked on failure location updates.
   *
   * @param message error description.
   */
  void onLocationAccessError(String message);

  /**
   * Handle onActivityResult after call ResolvableApiException.startResolutionForResult().
   *
   * @param resultCode 0 - user canceled reqest, -1 - user pressed "ok" (set High Accuracy mode).
   */
  void onGPSResolutionResult(int resultCode);

  void log(String text);
}