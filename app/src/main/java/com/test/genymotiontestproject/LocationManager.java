package com.test.genymotiontestproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;

/**
 * Location usage.
 */
public final class LocationManager {
  public static final int REQUEST_CODE_GPS_RESOLUTION = 3000;
  /**
   * The standard rate value for active location updates.
   * Exact. Updates will never be more frequent than this value.
   * 2 mins for standard work in background.
   */
  private static final long UPDATE_INTERVAL = 10_000;
  private static final String LOGTAG = "LOCATION";
  /**
   * Provides access to the Fused Location Provider API.
   */
  private FusedLocationProviderClient fusedLocationClient;
  /**
   * Provides access to the Location Settings API.
   */
  private SettingsClient settingsClient;
  /**
   * Stores parameters for requests to the FusedLocationProviderApi.
   */
  private LocationRequest locationRequest;
  /**
   * Stores the types of location services the client is interested in using.
   * Used for checking settings to determine if the device has optimal location settings.
   */
  private LocationSettingsRequest locationSettingsRequest;
  /**
   * Represents a geographical location.
   */
  private Location lastLocation;
  /**
   * Listener for pass location events to user interface.
   */
  private IOnLocationChangeListener onLocationChangeListener;
  /**
   * Callback for Location events.
   */
  private LocationCallback locationCallback;
  /**
   * On startLocationUpdates() failed we shedule its rerun.
   */
  private Handler startHandler;
  /**
   * Minimum distance (measured in meters) a device must move horizontally before an update event is generated.
   */
  private int minimalDistance;

  /**
   * Create new LocationManager.
   *
   * @param context application context for check permissions.
   */
  LocationManager(final Context context) {
    log("LocationManager()");
    minimalDistance = 100;

    fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    settingsClient = LocationServices.getSettingsClient(context);

    locationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(final LocationResult locationResult) {
        super.onLocationResult(locationResult);
        updateLocation(locationResult.getLastLocation());
      }
    };

    locationRequest = new LocationRequest();
    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    setLocationRequestInterval(UPDATE_INTERVAL);

    // Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
    // a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
    // if a device has the needed location settings.
    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
    builder.addLocationRequest(locationRequest);
    locationSettingsRequest = builder.build();

    if (Looper.myLooper() == null) {
      Looper.prepare();
    }
    startHandler = new Handler();
  }

  public static String locationToString(final Location location) {
    return location.getLatitude() + "," + location.getLongitude();
  }

  public boolean hasPermission(final Context context) {
    return PackageManager.PERMISSION_GRANTED
        == ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
  }

  /**
   * Sets up the location request. Android has two location request settings:
   * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
   * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
   * the AndroidManifest.xml.
   * <p/>
   * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
   * interval (5 seconds), the Fused Location Provider API returns location updates that are
   * accurate to within a few feet.
   * <p/>
   * These settings are appropriate for mapping applications that show real-time location
   * updates.
   *
   * @param interval interval for active location updates.
   */
  public void setLocationRequestInterval(final long interval) {
    log("setLocationRequestInterval " + interval);

    // Sets the desired interval for active location updates. This interval is
    // inexact. You may not receive updates at all if no location sources are available, or
    // you may receive them slower than requested. You may also receive updates faster than
    // requested if other applications are requesting location at a faster interval.
    locationRequest.setInterval(interval);

    // Sets the fastest rate for active location updates. This interval is exact, and your
    // application will never receive updates faster than this value.
    locationRequest.setFastestInterval(interval);
    // deliver to app immedeately.
    locationRequest.setMaxWaitTime(interval);
  }

  /**
   * Stop update lcation.
   */
  public void stopLocationUpdates() {
    log("stop updates");
    fusedLocationClient.removeLocationUpdates(locationCallback);
  }

  /**
   * Requests location updates from the FusedLocationApi.
   * Note: we don't call this unless location runtime permission has been granted.
   */
  @SuppressLint("MissingPermission")
  public void startLocationUpdates(final Activity activity) {
    log("startLocationUpdates() check location settings");
    // Begin by checking if the device has the necessary location settings.
    settingsClient.checkLocationSettings(locationSettingsRequest)
        .addOnSuccessListener((final LocationSettingsResponse locationSettingsResponse) -> {
              log("All location settings are satisfied.");
              startHandler.removeCallbacksAndMessages(null);
              fusedLocationClient.requestLocationUpdates(locationRequest,
                  locationCallback, Looper.myLooper());
            }
        )
        .addOnFailureListener((Exception e) -> {
              log("!!! checkLocationSettings onFailure " + e.getLocalizedMessage());
              if (e instanceof ResolvableApiException && activity != null) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                  // Show the dialog by calling startResolutionForResult(),
                  // and check the result in onActivityResult().
                  ((ResolvableApiException) e).startResolutionForResult(
                      activity,
                      REQUEST_CODE_GPS_RESOLUTION);

                } catch (IntentSender.SendIntentException sendEx) {
                  if (onLocationChangeListener != null) {
                    onLocationChangeListener.onLocationAccessError(e.getLocalizedMessage());
                  }
                }

              } else {
                if (onLocationChangeListener != null) {
                  onLocationChangeListener.onLocationAccessError(e.getLocalizedMessage());
                }

                // schedule restart
                log("Schedule restart");
                startHandler.removeCallbacksAndMessages(null);
                startHandler.postDelayed(() -> startLocationUpdates(activity), UPDATE_INTERVAL);
              }
            }
        );
  }

  /**
   * Apply new device location.
   * Check if new location is mocked or not different from previous.
   * If location is good - save it, update distances and call auto check in method.
   *
   * @param newLocation new device location
   */
  private void updateLocation(final Location newLocation) {
    if (newLocation == null) {
      Log.w(LOGTAG, "updateLocation() exit: new location is null");
      return;
    }

    log("Coordinates updated " + locationToString(newLocation));

    if (lastLocation != null && lastLocation.distanceTo(newLocation) < minimalDistance) {
      log("Location was not changed enough " + locationToString(lastLocation));
      return;
    }
    lastLocation = newLocation;

    if (onLocationChangeListener != null) {
      onLocationChangeListener.onLocationUpdated(newLocation);
    } else {
      log("onLocationChangeListener is null");
    }
  }

  /**
   * Get last known location.
   */
  @SuppressLint("MissingPermission")
  public void getLastKnownLocation() {
    log("getLastKnownLocation()");
    fusedLocationClient.getLastLocation()
        .addOnSuccessListener((final Location location) -> {
          // Got last known location. In some rare situations this can be null.
          log("getLastKnownLocation() onSuccess({})" + locationToString(location));
          updateLocation(location);
        });
  }

  /**
   * For activity subscribe on resume.
   *
   * @param onLocationChangeListener listener.
   */
  public void setOnLocationChangeListener(final IOnLocationChangeListener onLocationChangeListener) {
    log("setOnLocationChangeListener " + onLocationChangeListener);
    this.onLocationChangeListener = onLocationChangeListener;
  }

  /**
   * On pause activity must to unsubscribe.
   */
  public void removeOnLocationChangeListener() {
    log("removeOnLocationChangeListener()");
    onLocationChangeListener = null;
  }

  private void log(final String text) {
    if (onLocationChangeListener != null) {
      onLocationChangeListener.log(text);
    } else {
      Log.d(LOGTAG, text);
    }
  }
}