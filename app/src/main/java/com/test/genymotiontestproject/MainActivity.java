package com.test.genymotiontestproject;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements IOnLocationChangeListener {

  public static final int PERMISSIONS_REQUEST_LOCATION = 1110;
  private static final String LOGTAG = "MAIN";
  private LocationManager locationManager;
  private TextView searchingView;
  private TextView resultView;
  private TextView logView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    searchingView = findViewById(R.id.searching);
    resultView = findViewById(R.id.result);
    logView = findViewById(R.id.log);
    locationManager = new LocationManager(getApplicationContext());
  }

  @Override
  protected void onResume() {
    super.onResume();
    log("onResume()");
    locationManager.setOnLocationChangeListener(this);

    if (locationManager.hasPermission(getApplicationContext())) {
      locationManager.startLocationUpdates(this);
    } else {
      askForPermission();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    log("onPause()");

    locationManager.stopLocationUpdates();
    locationManager.removeOnLocationChangeListener();
  }

  @Override
  protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    log("onActivityResult requestCode=" + requestCode + " resultCode=" + resultCode);
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == LocationManager.REQUEST_CODE_GPS_RESOLUTION) {
      onGPSResolutionResult(resultCode);
    }
  }

  @Override
  public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions,
                                         @NonNull final int[] grantResults) {
    log("onRequestPermissionsResult " + requestCode);
    if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
      // If request is cancelled, the result arrays are empty.
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        locationManager.startLocationUpdates(this);
      }

    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void askForPermission() {
    ActivityCompat.requestPermissions(this,
        new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
        PERMISSIONS_REQUEST_LOCATION);
  }

  /**
   * Invoked every time location received.
   *
   * @param newLocation new location.
   */
  @Override
  public void onLocationUpdated(Location newLocation) {
    log("location updated: " + LocationManager.locationToString(newLocation));
    searchingView.setVisibility(View.GONE);
    resultView.setText(newLocation.getLatitude() + ", " + newLocation.getLongitude());
    resultView.setVisibility(View.VISIBLE);
  }

  /**
   * Invoked after onLocationUpdated if new location is different from last stored.
   *
   * @param newLocation really new location.
   */
  @Override
  public void onLocationChanged(Location newLocation) {
    log("location changed: " + LocationManager.locationToString(newLocation));
    searchingView.setVisibility(View.GONE);
    resultView.setText(newLocation.getLatitude() + ", " + newLocation.getLongitude());
    resultView.setVisibility(View.VISIBLE);
  }

  /**
   * Invoked when permissions required.
   */
  @Override
  public void onLocationPermissionRequired() {
    log("onLocationPermissionRequired");
    askForPermission();
  }

  /**
   * Invoked on failure location updates.
   *
   * @param message error description.
   */
  @Override
  public void onLocationAccessError(String message) {
    log("onLocationAccessError: " + message);
  }

  /**
   * Handle onActivityResult after call ResolvableApiException.startResolutionForResult().
   *
   * @param resultCode 0 - user canceled reqest, -1 - user pressed "ok" (set High Accuracy mode).
   */
  @Override
  public void onGPSResolutionResult(int resultCode) {
    log("onGPSResolutionResult: " + resultCode);
  }

  @Override
  public void log(final String text) {
    Log.d(LOGTAG, text);
    logView.append("\r\n" + text);
  }

}
