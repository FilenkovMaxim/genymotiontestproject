package com.test.genymotiontestproject.rules;

import android.os.Build;
import android.support.test.InstrumentationRegistry;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * This rule adds selected permissions to test app
 * Usage:
 *
 * @Rule public final AllowPermissionsRule locationPermissionsRule = new AllowPermissionsRule(
 * new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
 */

public class AllowPermissionsRule implements TestRule {

  private final String[] permissions;

  public AllowPermissionsRule(String[] permissions) {
    this.permissions = permissions;
  }

  @Override
  public Statement apply(final Statement base, Description description) {
    return new Statement() {
      @Override
      public void evaluate() throws Throwable {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          for (String permission : permissions) {
            getInstrumentation().getUiAutomation().executeShellCommand(
                "pm grant " + InstrumentationRegistry.getTargetContext().getPackageName()
                    + " " + permission);
          }
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            throw new RuntimeException("Cannot execute Thread.sleep()");
          }
        }

        base.evaluate();
      }
    };
  }
}