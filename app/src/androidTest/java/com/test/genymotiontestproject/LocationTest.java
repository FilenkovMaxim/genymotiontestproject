package com.test.genymotiontestproject;

import android.Manifest;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.genymotion.api.GenymotionManager;
import com.genymotion.api.Gps;
import com.test.genymotiontestproject.rules.AllowPermissionsRule;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;


/**
 * Test coordinates change.
 */
@LargeTest
@RunWith(Parameterized.class)
public class LocationTest {
  private static final String LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
  private static final String LOGTAG = "ESPRESSO_TEST";

  @Rule
  public final AllowPermissionsRule locationPermissionsRule
      = new AllowPermissionsRule(new String[] {LOCATION_PERMISSION});
  @Rule
  public ActivityTestRule<MainActivity> activityTR = new ActivityTestRule<>(MainActivity.class);

  private double latitude;
  private double longitude;

  /**
   * Parameterized constructor.
   */
  public LocationTest(final double latitude, final double longitude) {
    Log.d(LOGTAG, "========   LocationTest: " + latitude + ", " + longitude + "   =========");
    this.latitude = latitude;
    this.longitude = longitude;
    setLocation(InstrumentationRegistry.getTargetContext(), latitude, longitude);
  }

  private static void setLocation(final Context context, final double latitude, final double longitude) {
    if (!GenymotionManager.isGenymotionDevice()) {
      return;
    }

    Log.d(LOGTAG, "LocationTest setLocation() " + latitude + ", " + longitude);
    GenymotionManager.getGenymotionManager(context).getGps()
        .setAltitude(0)
        .setAccuracy(0)
        .setBearing(0)
        .setLatitude(latitude)
        .setLongitude(longitude)
        .setStatus(Gps.Status.ENABLED);
  }

  @BeforeClass
  public static void setup() {
    Log.d(LOGTAG, "LocationTest setup()");
    setLocation(InstrumentationRegistry.getTargetContext(), 81.70935, -75.71012);
  }

  @Parameterized.Parameters
  public static List<Object[]> data() {
    List<Object[]> parametres = new ArrayList<>();
    double[][] coordinates = {
        {77.631388, -86.780668},
        {81.709251, -75.715717},
        {78.586454, -101.923499}
    };
    for (int i = 0; i < coordinates.length; i++) {
      parametres.add(new Object[] {coordinates[i][0], coordinates[i][1]});
    }
    return parametres;
  }


  /**
   * Check coordinates with delay.
   * Always passed.
   */
//  @Test
  public void checkWithDelay() {
    Log.d(LOGTAG, "checkWithDelay");

    setLocation(InstrumentationRegistry.getTargetContext(), latitude, longitude);

    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      throw new RuntimeException("Cannot execute Thread.sleep()");
    }

    String expectedResult = latitude + ", " + longitude;

    onView(allOf(withId(R.id.result), isDisplayed()))
        .check(matches(withText(expectedResult)));

    Log.d(LOGTAG, "Test successfuly finished.");
  }

  /**
   * Check coordinates without delay.
   * Always failed.
   */
  @Test
  public void checkWithoutDelay() {
    Log.d(LOGTAG, "checkWithoutDelay");

    setLocation(InstrumentationRegistry.getTargetContext(), latitude, longitude);

    String expectedResult = latitude + ", " + longitude;

    onView(allOf(withId(R.id.result), isDisplayed()))
        .check(matches(withText(expectedResult)));

    Log.d(LOGTAG, "Test successfuly finished.");
  }
}